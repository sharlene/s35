// initialize database
const express = require("express")
const mongoose = require("mongoose")
const app = express()
const port = 3000

// initialize env file
require('dotenv').config()

// mongoose connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.6sl1kgl.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`, 
{
	useNewUrlParser:true,
	useUnifiedTopology:true
})

let db = mongoose.connection

// event listeners
db.on("error", () => console.log(`Connection error`))
db.once("open", () => console.log(`Connected to MongoDB!`))

// [section] schemas
const taskSchema = new mongoose.Schema({
    
    name : String,
    status: {
        type: String,
        default: "pending"
    }
})

const userSchema = new mongoose.Schema({
    
    username : String,
    password: String,
    status: {
        type: String
    }
})


// [section] model
const Task = mongoose.model("Task",taskSchema)
const User = mongoose.model("User",userSchema)

// middleware registration
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// [section] routes - tasks
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {
		if(result !== null && result.name == request.body.name) {
			return response.send("Duplicate task found!")
		} else {
			let new_task = new Task({
				name: request.body.name
			})

			new_task.save().then((created_task, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New task created!")
				}
			})
		}
	})
})

// get all tasks
app.get("/tasks", (request, response) => {
	Task.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}
		return response.status(200).send(result)
	})
})

// [section] routes - users
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}).then((result, error) => {
		if(result !== null && result.username == request.body.username) {
			return response.send("Duplicate user found!")
		} else {
			let new_user = new User({
				username: request.body.username,
				password: request.body.password
			})

			new_user.save().then((created_user, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New user created!")
				}
			})
		}
	})
})

// get all users
app.get("/users", (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}
		return response.status(200).send(result)
	})
})

app.listen(port, () => console.log(`The server is running at localhost:${port}`))
